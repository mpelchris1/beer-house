package gr.beer.orders.config;

import java.nio.charset.Charset;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.SSLContext;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.lang.NonNull;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

@Slf4j
@AllArgsConstructor
@Configuration
public class RestConfig {

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

  @SneakyThrows
  @Bean("beerRestTemplate")
  public RestTemplate beerRestTemplate() {
    TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
    SSLContext sslContext = SSLContexts
        .custom()
        .loadTrustMaterial(null, acceptingTrustStrategy)
        .build();

    SSLConnectionSocketFactory sslContextFactory = new SSLConnectionSocketFactory(sslContext);

    BufferingClientHttpRequestFactory requestFactory = new BufferingClientHttpRequestFactory(new HttpComponentsClientHttpRequestFactory(
        HttpClients.custom()
            .setSSLSocketFactory(sslContextFactory)
            .build()));

    RestTemplate template = new RestTemplateBuilder()
        .requestFactory(() -> requestFactory)
        .build();

    template.setInterceptors(List.of((httpRequest, bytes, clientHttpRequestExecution) -> {
      HttpHeaders httpHeaders = httpRequest.getHeaders();
      return clientHttpRequestExecution.execute(httpRequest, bytes);
    }));

    template.setErrorHandler(new ResponseErrorHandler() {


      @SneakyThrows
      @Override
      public boolean hasError(@NonNull ClientHttpResponse clientHttpResponse) {
        return !(clientHttpResponse.getStatusCode().is2xxSuccessful());
      }

      @SneakyThrows
      @Override
      public void handleError(@NonNull ClientHttpResponse clientHttpResponse) {
        String responseString = StreamUtils.copyToString(clientHttpResponse.getBody(), Charset.defaultCharset());
        throw new Exception(
            String.format("Received Unexpected Error from Facetec. Status [%s] | Body [%s]",
                clientHttpResponse.getStatusCode().value(),
                responseString
            ));
      }
    });
    return template;
  }

}
