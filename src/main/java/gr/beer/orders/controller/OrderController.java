package gr.beer.orders.controller;

import gr.beer.orders.domain.GenericResponse;
import gr.beer.orders.domain.SuccessOperation;
import gr.beer.orders.domain.order.OrderRequest;
import gr.beer.orders.entity.Order;
import gr.beer.orders.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@AllArgsConstructor
@RequestMapping("/order")
public class OrderController {

  private final OrderService orderService;

  @PostMapping(value = "/new")
  public GenericResponse<SuccessOperation> orderNewBeer(
      @RequestHeader(value = "x-table-id") String tableId,
      @RequestBody OrderRequest orderRequest) {

    return GenericResponse.fromData(orderService.newOrder(tableId, orderRequest));
  }

  @PutMapping(value = "/{orderId}/update")
  public GenericResponse<SuccessOperation> updateOrder(
      @PathVariable(value = "orderId") String orderId,
      @RequestBody OrderRequest orderRequest) {

    return GenericResponse.fromData(orderService.updateOrder(orderId, orderRequest));
  }

  @PutMapping(value = "/{orderId}/requestBill")
  public GenericResponse<SuccessOperation> requestBuill(
      @PathVariable(value = "orderId") String orderId) {

    return GenericResponse.fromData(orderService.requestBill(orderId));
  }

  @PutMapping(value = "/{orderId}/close")
  public GenericResponse<SuccessOperation> closeOrderBill(
      @PathVariable(value = "orderId") String orderId) {

    return GenericResponse.fromData(orderService.closeBill(orderId));
  }

  @GetMapping(value = "/{orderId}/get")
  public GenericResponse<Order> getOrder(
      @PathVariable(value = "orderId") String orderId) {

    return GenericResponse.fromData(orderService.getOrder(orderId));
  }


}
