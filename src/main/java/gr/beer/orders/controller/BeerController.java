package gr.beer.orders.controller;

import gr.beer.orders.domain.BeerResponse;
import gr.beer.orders.domain.GenericResponse;
import gr.beer.orders.service.BeerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(value = "/beers")
public class BeerController {

  private final BeerService beerService;

  @GetMapping(value = "/all")
  public GenericResponse<BeerResponse> getBeersByClient(
      @RequestHeader(value = "x-table-id") String tableId,
      @RequestParam(value = "size", required = false, defaultValue = "10") String size) {
    log.info("Entered /beer/all with tableId = {}", tableId);

    return GenericResponse.fromData(beerService.getAllBeers(size));
  }

  @GetMapping(value = "/inpub")
  public GenericResponse<BeerResponse> getBeersInPub(
      @RequestHeader(value = "x-table-id") String tableId,
      @RequestParam(value = "size", required = false, defaultValue = "10") String size) {
    log.info("Entered /beer/inpub with tableId = {}", tableId);

    return GenericResponse.fromData(beerService.getBeersInPub(size));
  }
}
