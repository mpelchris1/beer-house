package gr.beer.orders.repository;

import gr.beer.orders.entity.Beer;
import java.awt.print.Pageable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BeerRepository extends PagingAndSortingRepository<Beer, Long> {

   List<Beer> findAllByUid(String uid);

  // int findPriceById(Long id);


}
