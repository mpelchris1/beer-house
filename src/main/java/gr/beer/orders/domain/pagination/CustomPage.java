package gr.beer.orders.domain.pagination;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomPage<E> {

  private int pageNumber;
  private int totalPages;
  private long totalElements;
  private List<E> pageItems = new ArrayList<E>();

}
