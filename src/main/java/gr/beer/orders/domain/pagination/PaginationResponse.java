package gr.beer.orders.domain.pagination;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A form used for pagination
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaginationResponse implements Serializable {

  private Integer self;
  private Long total;
  private Integer next;
  private Integer prev;

}
