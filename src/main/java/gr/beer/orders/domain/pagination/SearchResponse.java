package gr.beer.orders.domain.pagination;

import com.fasterxml.jackson.annotation.JsonProperty;
import gr.beer.orders.domain.ErrorResponse;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchResponse<T> {

  @JsonProperty("data")
  List<T> data;

  @JsonProperty("exceptions")
  @Builder.Default
  private ErrorResponse exceptions = ErrorResponse.builder().build();

  @JsonProperty("pagination")
  private PaginationResponse pagination;

  @SuppressWarnings("unchecked")
  public static <T> SearchResponse<T> fromPage(CustomPage customPage) {
    return SearchResponse.<T>builder()
        .data(customPage.getPageItems())
        .pagination(PaginationResponse.builder()
            .self(customPage.getPageNumber())
            .total(customPage.getTotalElements())
            .build())
        .build();
  }

}
