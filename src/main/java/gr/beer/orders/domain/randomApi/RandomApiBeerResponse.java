package gr.beer.orders.domain.randomApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Random;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RandomApiBeerResponse {

  private int id;
  private String uid;
  private String brand;
  private String name;
  private String style;
  private String hop;
  private String yeast;
  private String malts;
  private String ibu;
  private String alcohol;
  private String blg;


  private int price = getPrice();

 public int getPrice() {
  return new Random().nextInt(10) + 5;
 }
}
