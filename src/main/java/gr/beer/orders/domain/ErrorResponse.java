package gr.beer.orders.domain;

import static java.util.Objects.nonNull;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Collections;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {

  @JsonProperty("errors")
  @Builder.Default
  List<ErrorDetails> errors = Collections.emptyList();

  @JsonProperty("total")
  private int total;

  @JsonProperty("total")
  public int getTotal() {
    return nonNull(errors) ? errors.size() : 0;
  }
}
