package gr.beer.orders.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonPropertyOrder({
    "code",
    "title",
    "description",
    "exceptionCode",
    "debug",
    "redirect",
    "datetime"
})
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDetails {

  @JsonProperty("code")
  private int httpStatus;

  @JsonProperty("debug")
  private String debug;

  @JsonProperty("title")
  private String title;

  @JsonProperty("description")
  private String description;

  @JsonProperty("exceptionCode")
  private String businessCode;

  @JsonProperty("redirect")
  private String redirect;

  @JsonProperty("datetime")
  private ZonedDateTime timestamp;
}
