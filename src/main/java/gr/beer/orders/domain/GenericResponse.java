package gr.beer.orders.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.Arrays;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonPropertyOrder({
    "data",
    "exceptions"
})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GenericResponse<T> {

  @JsonProperty("data")
  T data;

  @JsonProperty("exceptions")
  @Builder.Default
  private ErrorResponse exceptions = ErrorResponse.builder().build();

  public static GenericResponse<SuccessOperation> buildSuccess(boolean result) {
    return GenericResponse.<SuccessOperation>builder().data(SuccessOperation.builder().success(result).build()).build();
  }

  public static GenericResponse<SuccessOperation> buildSuccess(Runnable runnable) {
    runnable.run();
    return buildSuccess(true);
  }

  public static GenericResponse<Object> fromErrorDetails(ErrorDetails... errorDetails) {
    return GenericResponse.<Object>builder()
        .exceptions(ErrorResponse.builder().errors(Arrays.stream(errorDetails).collect(Collectors.toList())).build())
        .build();
  }

  public static <E> GenericResponse<E> fromData(E data) {
    return GenericResponse.<E>builder()
        .data(data)
        .build();
  }

}
