package gr.beer.orders.domain.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BeerRequest {

  private Long beerId;
  private int quantity;

}
