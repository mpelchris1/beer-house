package gr.beer.orders.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import gr.beer.orders.domain.randomApi.RandomApiBeerResponse;
import gr.beer.orders.entity.Beer;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BeerResponse {

  private List<RandomApiBeerResponse> beerList;
  private List<Beer> beersInPubList;
  private int total;

}
