package gr.beer.orders.client;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.REQUEST_TIMEOUT;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import gr.beer.orders.domain.RandomApiErrorResponse;
import gr.beer.orders.domain.randomApi.RandomApiBeerResponse;
import gr.beer.orders.exception.RandomApiClientException;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Component
public class BeerClient {

  public static final List<HttpStatus> EXPECTED_ERRORS = ImmutableList.of(UNAUTHORIZED, FORBIDDEN, NOT_FOUND, REQUEST_TIMEOUT, INTERNAL_SERVER_ERROR);
  private static final ObjectMapper MAPPER = new ObjectMapper();

  private final RestTemplate restTemplate;
  private final ConversionService conversionService;
  private final String getBeersRequestEndpoint;

  @Autowired
  public BeerClient(@Qualifier("beerRestTemplate") RestTemplate beerRestTemplate, ConversionService conversionService) {
    this.restTemplate = beerRestTemplate;
    this.conversionService = conversionService;
    this.getBeersRequestEndpoint = "https://random-data-api.com/api/beer/random_beer";
  }

  @SneakyThrows
  public List<RandomApiBeerResponse> sendGetBeers(String size) {

    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getBeersRequestEndpoint).queryParam("size", size);
    ResponseEntity<String> response = safeExecute(() -> restTemplate.getForEntity(builder.build().encode().toUri(), String.class));

    return MAPPER.readValue(Objects.requireNonNull(response.getBody()), new TypeReference<>() {
    });
  }

  private ResponseEntity<String> safeExecute(Supplier<ResponseEntity<String>> requestSupplier) {
    ResponseEntity<String> responseEntity = requestSupplier.get();
    if (EXPECTED_ERRORS.contains(responseEntity.getStatusCode())) {
      log.error("Received error from RandomApi: [{}]", responseEntity.getBody());
      RandomApiErrorResponse randomApiErrorResponse;
      try {
        randomApiErrorResponse = conversionService.convert(responseEntity.getBody(), RandomApiErrorResponse.class);
      } catch (Exception e) {
        randomApiErrorResponse = RandomApiErrorResponse.builder().build();
      }
      throw new RandomApiClientException(
          String.format("Received error from RandomApi: [%s]", responseEntity.getStatusCode().value()),
          responseEntity.getStatusCode(), randomApiErrorResponse);
    }
    return responseEntity;
  }
}
