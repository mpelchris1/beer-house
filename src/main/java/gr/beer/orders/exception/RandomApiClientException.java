package gr.beer.orders.exception;

import gr.beer.orders.domain.RandomApiErrorResponse;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class RandomApiClientException extends RuntimeException {

  private final HttpStatus errorStatus;
  private final RandomApiErrorResponse randomApiErrorResponse;

  public RandomApiClientException(String message, HttpStatus errorStatus, RandomApiErrorResponse randomApiErrorResponse) {
    this.errorStatus = errorStatus;
    this.randomApiErrorResponse = randomApiErrorResponse;
  }
}
