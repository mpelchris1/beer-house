package gr.beer.orders.exception.handler;

import gr.beer.orders.domain.ErrorDetails;
import gr.beer.orders.domain.ErrorResponse;
import gr.beer.orders.domain.GenericResponse;
import java.time.ZonedDateTime;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
public class ExceptionHandler {

  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @org.springframework.web.bind.annotation.ExceptionHandler({HttpMediaTypeNotSupportedException.class, MethodArgumentTypeMismatchException.class, HttpMessageNotReadableException.class, MissingRequestHeaderException.class, MissingServletRequestParameterException.class, HttpMediaTypeNotAcceptableException.class, HttpRequestMethodNotSupportedException.class})
  public GenericResponse<Object> handleBadRequests(final Exception ex, WebRequest request) {
    return GenericResponse.builder().exceptions(ErrorResponse.builder().errors(
        List.of(ErrorDetails
            .builder().title(ex.getClass().getSimpleName()).description(request.getDescription(false)).httpStatus(HttpStatus.BAD_REQUEST.value()).timestamp(ZonedDateTime.now()).debug(ex.getMessage()).build())).build()).build();
  }
}
