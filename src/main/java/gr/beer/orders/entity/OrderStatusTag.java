package gr.beer.orders.entity;

import java.util.Arrays;

public enum OrderStatusTag {

  PENDING("pending"), REQUESTED_BILL_FROM_USER("requested_bill_from_user"), CLOSED("closed");

  private String value;

  OrderStatusTag(String value) {
    this.value = value;
  }

  public static OrderStatusTag fromString(String s) throws IllegalArgumentException {
    return Arrays.stream(OrderStatusTag.values())
        .filter(v -> v.value.equals(s))
        .findFirst()
        .orElseThrow(() -> new IllegalArgumentException("unknown value: " + s));
  }

  public String getValue() {
    return value;
  }
}
