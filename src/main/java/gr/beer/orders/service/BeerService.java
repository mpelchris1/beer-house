package gr.beer.orders.service;

import gr.beer.orders.client.BeerClient;
import gr.beer.orders.domain.BeerResponse;
import gr.beer.orders.domain.randomApi.RandomApiBeerResponse;
import gr.beer.orders.entity.Beer;
import gr.beer.orders.repository.BeerRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@AllArgsConstructor
public class BeerService {

  private final BeerClient beerClient;
  private final BeerRepository repository;

  public BeerResponse getAllBeers(String size) {

    List<RandomApiBeerResponse> beerList = beerClient.sendGetBeers(size);

    beerList.stream()
        .filter((b) -> repository.findAllByUid(b.getUid()).isEmpty())
        .forEach(
            (b) -> {
              repository.save(Beer.builder()
                  .uid(b.getUid())
                  .alcohol(b.getAlcohol())
                  .blg(b.getBlg())
                  .brand(b.getBrand())
                  .hop(b.getHop())
                  .malts(b.getMalts())
                  .name(b.getName())
                  .style(b.getStyle())
                  .ibu(b.getIbu())
                  .price(b.getPrice())
                  .yeast(b.getYeast())
                  .build());
              log.info("Saved beer : {}", b);
            }

        );
    return BeerResponse.builder().beerList(beerList).total(beerList.size()).build();
  }

  public BeerResponse getBeersInPub(String size) {
    Page<Beer> beersInPubList =  repository.findAll(PageRequest.of(0, Integer.parseInt(size)));

    return BeerResponse.builder().beersInPubList(beersInPubList.toList()).total(beersInPubList.getNumberOfElements()).build();
  }
}
