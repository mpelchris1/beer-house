package gr.beer.orders.service;

import gr.beer.orders.domain.SuccessOperation;
import gr.beer.orders.domain.order.BeerRequest;
import gr.beer.orders.domain.order.OrderRequest;
import gr.beer.orders.entity.BeerWithQuantity;
import gr.beer.orders.entity.Order;
import gr.beer.orders.entity.OrderStatusTag;
import gr.beer.orders.repository.BeerRepository;
import gr.beer.orders.repository.OrderRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@AllArgsConstructor
@Service
public class OrderService {

  private final OrderRepository orderRepository;
  private final BeerRepository beerRepository;

  public SuccessOperation newOrder(String tableId, OrderRequest orderRequest) {

    saveOrder(tableId, orderRequest);

    return SuccessOperation.builder().success(true).build();
  }

  private void saveOrder(String tableId, OrderRequest orderRequest) {

    List<BeerWithQuantity> beerInDbList = new ArrayList<>();
    Order order =
        Order.builder()
            .beers(orderRequest.getBeerRequest() != null ? getBeerWithQuantityList(orderRequest.getBeerRequest(), beerInDbList) : null)
            .orderStatusTag(OrderStatusTag.PENDING.getValue())
            .tableId(tableId)
            .build();
    orderRepository.save(order);
    log.info("Saved order: {}", order);
  }

  private List<BeerWithQuantity> getBeerWithQuantityList(List<BeerRequest> beerRequestList,
                                                         List<BeerWithQuantity> beerInDbList) {

    List<BeerWithQuantity> updatedBeerListInDbList = new ArrayList<>(beerInDbList);

    if (beerInDbList.isEmpty()) {
      beerRequestList.forEach(
          (beerRequestItem) -> {
            addBeerwithQuantity(beerRequestItem, beerInDbList);
          });
      return beerInDbList;
    } else {
      getUpdatedBeerList(beerRequestList, beerInDbList, updatedBeerListInDbList);
    }
    return updatedBeerListInDbList;
  }

  private List<BeerWithQuantity> getUpdatedBeerList(List<BeerRequest> beerRequestList, List<BeerWithQuantity> beerInDbList,
                                                    List<BeerWithQuantity> updatedBeerListInDbList) {
    int quantity;
    for (BeerRequest beerRequestItem : beerRequestList) {

      int index = getIndexOfValueInList(beerInDbList, beerRequestItem);
      if (index != -1) {
        quantity = Math.addExact(beerRequestItem.getQuantity(), beerInDbList.get(index).getQuantity());
        updatedBeerListInDbList.get(index).setQuantity(quantity);
      } else {
        addBeerwithQuantity(beerRequestItem, updatedBeerListInDbList);
      }
    }
    return updatedBeerListInDbList;
  }

  private int getIndexOfValueInList(List<BeerWithQuantity> beerInDbList, BeerRequest beerRequestItem) {

    return IntStream.range(0, beerInDbList.size())
        .filter((x) -> beerInDbList.get(x).getBeerId().equals(beerRequestItem.getBeerId()))
        .findFirst().orElse(-1);
  }

  private void addBeerwithQuantity(BeerRequest beerRequestItem, List<BeerWithQuantity> updatedBeerListInDbList) {
    updatedBeerListInDbList.add(BeerWithQuantity.builder()
        .beerId(beerRequestItem.getBeerId())
        .quantity(beerRequestItem.getQuantity()).build());
  }

  public SuccessOperation updateOrder(String orderId, OrderRequest orderRequest) {

    orderRepository.findById(Long.parseLong(orderId))
        .ifPresent((order) -> updateAndSaveOrder(order, orderRequest)
        );

    return SuccessOperation.builder().success(true).build();
  }

  private void updateAndSaveOrder(Order order, OrderRequest orderRequest) {

    List<BeerWithQuantity> beerWithQuantityList = order.getBeers() != null ? order.getBeers() : new ArrayList<>();
    order.setBeers(orderRequest.getBeerRequest() != null ?
        getBeerWithQuantityList(orderRequest.getBeerRequest(), beerWithQuantityList) : null);
    orderRepository.save(order);
    log.info("Updated order: {}", order);

  }


  public SuccessOperation requestBill(String orderId) {
    Optional<Order> optionalOrder = orderRepository.findById(Long.parseLong(orderId));
    if (optionalOrder.isPresent()) {
      Order order = optionalOrder.get();
      order.setOrderStatusTag(OrderStatusTag.REQUESTED_BILL_FROM_USER.getValue());
      orderRepository.save(order);
    }

    return SuccessOperation.builder().success(optionalOrder.isPresent()).build();
  }

  public SuccessOperation closeBill(String orderId) {

    orderRepository.findById(Long.parseLong(orderId))
        .ifPresent((order) -> {
              order.setOrderStatusTag(OrderStatusTag.CLOSED.getValue());
              orderRepository.save(order);
            }
        );
    return SuccessOperation.builder().success(true).build();
  }


  public Order getOrder(String orderId) {

    Optional<Order> optionalOrder = orderRepository.findById(Long.parseLong(orderId));

    return optionalOrder.orElse(null);
  }
}